# demo main project

1. a little bit about myself
2. i have been hired to help you to improve the devops workflow and pipelines

# workflow

Everthing starts from Jira ticket
1. Jira ticket
  - needs to have semantic prefixes
  - the ticket is about the fix then prefix should reflect this
  - similarly if the ticket is about the feature it prefix should reflect it as well
  - this will help with versioning (about this in a bit)

2. goal is to make it very easy to create a topic branch and Gitlab merge request via jira ticket

3. when branch and merge request is created, developer can checkout to topic branch and start working
  - merge request will be not ready state / draft
  - show the merge request

4. create some changes in the app and push it / show commit stage
  - commit stage goal is to run under 10 minutes for fast feedback
  - commit stage is only for static analysis like unit tests, code quality checks, Static Application Security Testing etc

5. When developer is done coding and feels like the bug or feature is ready to be released
  - request the code review from the collegues and/or tech leads

6. if all the feedback is gathered and suggestions implemented, discussions are resolved and approved
  - we can mark merge request as ready

7. next step is up for the debate but its highly encourage to squash the commit 
  - it helps with the semantic versioning and release notes

8. right now its important to clean up the squash commit message but we will fix this soon

9. we are ready to merge!

10. present the acceptance stage
  - we will have e2e tests for chrome and firefox

11. we also have release there
  