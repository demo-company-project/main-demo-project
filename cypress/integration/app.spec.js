describe('Main Page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3342/');
  });

  it('should have a page header', () => {
    cy.get('h2').contains('FBI Wanted');
  });

  it('should have a button', () => {
    cy.get('button').contains('Get next 20 wanted');
  });

  it('should have a list of 20 fugitives', () => {
    cy.get('[data-cy="list"]').children().should('have.length', 20);
  });

  // it('should fetch new list of 20 fugitives', () => {
  //   cy.get('ul>li').contains('UNKNOWN INDIVIDUAL - JOHN DOE 45');
  //   cy.get('[data-cy="list"]').children().should('have.length', 20);
  //   cy.get('button').click();
  //   cy.get('[data-cy="list"]').children().should('have.length', 20);
  //   cy.get('ul>li').should('not.contain', 'UNKNOWN INDIVIDUAL - JOHN DOE 45');
  // });
});
