export default function Dropdown({ filter, onChange }) {
  return (
    <div className="max-w-lg mx-auto">
      <select
        id="location"
        name="location"
        onChange={(e) => onChange(e.target.value)}
        className="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md"
      >
          {
              filter.map(item => {
                  return (
                      <option key={item.title} value={item.title}>{item.title}</option>
                  )
              })
          }
      </select>
    </div>
  );
}
