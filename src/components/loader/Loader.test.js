import { render, screen, getByTestId } from '@testing-library/react';
import Loader from './Loader';

test('Should show the loader', () => {
  render(<Loader loading={true} />);
  const element = screen.getByTestId('loader');
  expect(element).toBeInTheDocument();
});

test('Should not show the loader', () => {
  render(<Loader loading={false} />);
  const element = screen.queryByTestId('loader');
  expect(element).not.toBeInTheDocument();
});
