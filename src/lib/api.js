export async function getFugitives(rand) {
  try {
    const res = await fetch(`https://api.fbi.gov/wanted/v1/list?page=${rand}`, {
      method: 'GET',
    });
    const criminals = await res.json();
    return criminals;
  } catch (err) {
    console.error(err);
  }
}

// export async function getFugitives(rand, filter) {
//   try {
//     const res = await fetch(`https://api.fbi.gov/wanted/v1/list?page=${rand}?sex=${filter}`, {
//       method: 'GET',
//     });
//     const criminals = await res.json();
//     return criminals;
//   } catch (err) {
//     console.error(err);
//   }
// }

