import { useState } from "react";
import { getFugitives } from "../lib/api";
import { getRandomNumber } from "@demo-company-project/get-rando";
import { Loader } from "../components/loader";

export default function Home({ fugitives }) {
  const [criminals, setCriminals] = useState(fugitives);
  const [loading, setLoading] = useState(false);
  const [filter, setFilter] = useState('All')

  const getNewRandoms = async () => {
    setLoading(true);
    const rand = await getRandomNumber(1, 20, criminals.total);
    const get = await getFugitives(rand, filter);
    setCriminals(get);
    setLoading(false);
  };

  const filterWantedBySex = async (filter) => {
    setLoading(true);
    setFilter(filter)
    const rand = await getRandomNumber(1, 20, criminals.total);
    const get = await getFugitives(rand, filter);
    setCriminals(get);
    setLoading(false);
  }

  return (
    <div className="bg-white dark:bg-gray-900 h-auto">
      <div className="max-w-7xl mx-auto py-12 px-4 text-center sm:px-6 lg:px-8 lg:py-24">
        <div className="space-y-12">
          <div className="space-y-5 sm:mx-auto sm:max-w-xl sm:space-y-4 lg:max-w-5xl">
            <h2 className="p-8 text-3xl font-extrabold tracking-tight sm:text-4xl dark:text-white">
              FBI Wanted
            </h2>
            <button
              onClick={getNewRandoms}
              type="button"
              className="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              Get next 20 wanted
            </button>
          </div>
          {loading ? (
            <Loader loading={loading} />
          ) : (
            <ul
              role="list"
              data-cy="list"
              className="mx-auto space-y-16 sm:grid sm:grid-cols-2 sm:gap-16 sm:space-y-0 lg:grid-cols-3 lg:max-w-5xl"
            >
              {criminals.items.map((criminal) => (
                <li key={criminal.uid} data-cy="list-item">
                  <div className="space-y-6">
                    <img
                      className="mx-auto h-40 w-40 rounded-full xl:w-56 xl:h-56"
                      src={criminal.images[0].thumb}
                      alt=""
                    />
                    <div className="space-y-2">
                      <div className="text-lg leading-6 font-small space-y-1 dark:text-white">
                        <h3>{criminal.title}</h3>
                        <p className="text-sm text-gray-600 dark:text-white">
                          {criminal.description}
                        </p>
                      </div>
                      <ul role="list" className="flex justify-center space-x-5">
                        <li>
                          <a
                            href={criminal.files[0].url}
                            className="text-gray-400 hover:text-gray-500 dark:text-white dark:hover:text-gray-200"
                            target="_blank"
                            rel="noreferrer"
                          >
                            <span className="sr-only">Twitter</span>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="h-6 w-6"
                              fill="none"
                              viewBox="0 0 24 24"
                              stroke="currentColor"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"
                              />
                            </svg>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </li>
              ))}
            </ul>
          )}
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps(context) {
  const fugitives = await getFugitives();

  return {
    props: {
      fugitives,
    },
  };
}
